// Napisati program na programskom jeziku C koji �ifruje poruku pro�itanu sa standardnog ulaza.
// Poruka se zadaje u jednom redu.Program �ita i transformi�e znak po znak poruke sve dok ne stigne
// do znaka za novi red.Transformacija svakog znaka poruke se vr�i na slede�i na�in : izgeneri�e se
// slu�ajan ceo broj u opsegu[1, 255], zatim se izvr�i bitska operacija �ekskluzivno ili� slu�ajnog broja i
// koda znaka i na kraju se dobijeni me�urezultat zarotira ulevo za onoliko mesta koliko slu�ajan broj
// ima jedinica(najvi�i bit se uvek premesti na mesto najni�eg bita prilikom rotacije za jedno mesto
// ulevo).Nakon transformacije, dobijena znakovna vrednost se ispisuje na standardni izlaz u
// heksadecimalnom formatu.

// scanf, printf
#include <stdio.h>

// rand
#include <stdlib.h>

// maksimalna duzina poruke koja se sifruje
#define MAX 200

int main()
{
    // slucajni broj treba da bude od 1 do 255, jer se kombinuje sa char-ovima poruke
    // signed char opseg vrednosti je [-128, 127]
    // unsigned char opseg vrednosti je [0, 255]
    // char opseg vrednosti zavisi od implementacije, pa za svaki slucaj necemo da koristimo char
    // za slucajni broj pri generisanju
    // kasnije, kad ga ispravno generisemo u datom opsegu, mozemo da ga cast-ujemo u bilo koji char
    // tip
    // dodatna korist je sto i druge vrednosti (van opsega) mogu da stanu u int, pa mozemo da
    // proverimo da li je korisnik uneo odgovarajucu vrednost, a da ne dodje do zaokruzivanja
    // broja na 8 bita, koliko imaju char podaci, pre provere da li je broj stvarno u opsegu
    int slucajanBroj;

    // Broj jedinica u slucajnom broju. Koristi se da toliko puta ulevo rotiramo znak poruke pri sifrovanju.
    int brojJedinica;

    // Bit maska koju koristimo pri brojanju jedinica u slucajnom broju.
    int bitMaska;

    // MAX znakova je za poruku, dodatni je za terminalni simbol '\0'.
    // Ovo obezbedjuje da imamo MAX znakova za korisnicki sadrzaj poruke, ne racunajuci taj simbol,
    // a takodje smo sigurni da imamo dovoljno mesta i za njega.
    char poruka[MAX + 1];
    int n;  // duzina poruke
    int i, j;

    ////////////////////////////////////////////////////////////////////
    ////////// 1. Citanje poruke sa standardnog ulaza //////////////////
    ////////////////////////////////////////////////////////////////////
    printf("Unesite poruku koju treba sifrovati:\n");
    n = 0;
    while (n < MAX)
    {
        // Ova petlja cita znak po znak poruke sa standardnog ulaza. Najvise mozemo da ucitamo
        // MAX znakova, a posebno smo alocirali dodatno mesto za terminalni znak '\0'.
        // Mogli smo da stavimo da je poruka duzine MAX, ali onda bismo citali najvise MAX - 1 znakova.
        // Petlja bi bila while (n < MAX - 1).
        // Osim toga, ako ucitamo znak za novi red '\n' (unosi se pritiskom na taster ENTER),
        // takodje treba da iskocimo iz petlje i tad radimo break.

        // getchar() vraca int, pa ga cast-ujem u char jer je poruka niz char-ova.
        char ch = (char)getchar();
        if (ch != '\n')
        {
            // Ako nismo dosli do kraja poruke, upisi procitani znak i predji na indeks
            // sledeceg znaka u poruci.
            poruka[n] = ch;
            ++n;
        }
        else
        {
            // Dosli smo do kraja poruke, obustavljamo dalje citanje.
            break;
        }
    }

    // Nebitno da li smo iskocili iz petlje zbog nedostatka mesta za znakove u memoriji ili
    // zbog citanja znaka za novi red, potrebno je na kraj poruke ubaciti terminalni znak '\0'.
    // Sigurni smo da imamo dovoljno memorije jer smo tako alocirali poruku.
    poruka[n] = '\0';

    // strlen(poruka) vraca broj znakova od zadatog, pa do terminalnog simbola, sto je isto sto
    // i pozicija/indeks terminalnog simbola. Zato nema potrebe citati duzinu stringa sa strlen,
    // jer n vec ima tu vrednost. Ne samo da nema potrebe, nego ne smemo da koristimo strlen, strcpy,
    // strcat i slicne funkcije dok ne upisemo terminalni simbol jer one racunaju da on vec postoji upisan!

    ////////////////////////////////////////////////////////////////////
    ////////// 2. Transformacija svakog znaka poruke slucajnim brojem //
    ////////////////////////////////////////////////////////////////////

    // Generisemo slucajan broj.
    // formula je min + (max - min) * random[0, 1)
    // [1,255] se dobije kao 1 + (255 - 1) * random[0, 1)
    // random[0, 1) se dobija kao rand() / (double)RAND_MAX
    // rand() vraca vrednost od 0 do 32767, a RAND_MAX je 32768, znaci nikad ne mozemo da dobijemo 1.0.
    // Zato sam za opseg stavio [0,1)
    // Ovo radi za realne i cele brojeve.
    // Precica za cele brojeve je min + rand() % (MAX - MIN + 1).
    // Npr. za broj izmedju 5 i 25, racunajuci 5 i 25, je 5 + rand() % (25 - 5 + 1)
    // tj. 5 + rand() % 21, pri cemu je rand() % 21 nesto izmedju 0 i 20, pa je konacni rezultat izmedju 5 i 25

    // Opsti primer:
    //slucajanBroj = (int)(1 + (255 - 1) * rand() / (double)RAND_MAX);

    // Primer za cele brojeve:
    slucajanBroj = 1 + rand() % (255 - 1 + 1);

    // Broj jedinica se odredi bit maskom.
    // Posto je slucajanBroj > 0 i slucajanBroj <= 255, a 255 je 2^8 - 1 (odnosno najnizih 8 bita su jedinice),
    // to znaci da je dovoljno proveriti sve bite od 2^0 do 2^7.
    // To mozemo lako da uradimo pomocu bit maske koja pocinje od 2^7, pa je siftujemo desno jedan po jedan bit,
    // a svaki put proverimo da li vazi (maska & slucajanBroj) != 0. Ako je tako, onda na trenutnom bitu
    // slucajanBroj ima jedinicu, pa inkrementiramo brojJedinica.
    brojJedinica = 0;
    bitMaska = 0x80;    // ovo je 2^7 == 128 == 1000 0000 binarno == 0x80 heksadecimalno
    while (bitMaska != 0)
    {
        // Dok ne proverimo sve bite od 2^7 do 2^0, vrti petlju.
        if ((bitMaska & slucajanBroj) != 0)
        {
            ++brojJedinica;
        }

        // Pomeri bit masku.
        // alternativa je bitmaska /= 2; ali je malo bolje koristiti siftovanje za pomeranje bit maske
        bitMaska >>= 1;
    }

    // Drugi nacin za proveru broja jedinica u slucajnom broju je da gledamo najnizi bit, odnosno
    // da li je broj paran ili neparan. Tad broj siftujemo desno jednom ili ga podelimo sa 2
    // u svakom prolazu kroz petlju. Npr.:
    //pomBroj = slucajanBroj;
    //while (pomBroj != 0)
    //{
    //    if (pomBroj % 2)
    //    {
    //        // neparan broj, najnizi bit je 1
    //        ++brojJedinica;
    //    }

    //    // Odabrati jedno od sledeca dva:
    //    // ILI
    //    pomBroj /= 2;
    //    // ILI
    //    pomBroj >>= 1;
    //}

    for (i = 0; i < n; ++i)
    {
        // Transformacija znaka poruka[i].
        char ch = poruka[i];

        // 1. korak - XOR (exclusive or / ekskluzivno ili)
        // cast-ujemo u char jer char ima 8 bita. Sigurni smo da moze slucajanBroj da stane u char tip
        // jer mu je vrednost izmedju 0 i 255, moze da stane na 8 bita.
        char sifra = (char)slucajanBroj ^ ch;

        // 2. korak - rotiranje ulevo
        // Rotiranje, zato sto u postavci zadatka kaze da se najvisi bit posle siftovanja ubacuje
        // na najnizi bit. Moramo prvo da ga zapamtimo, pa posle rucno ubacimo na najnizi bit.
        // Posto je siftovanje ulevo, nije nam bitno da li je char na kompajleru koji koristimo
        // signed char ili unsigned char. Bilo bi bitno pri siftovanju desno. Tad bismo morali
        // da cast-ujemo u (signed char) ili (unsigned char) da znamo da li ce na najvisem bitu
        // ispravno da se ubaci 0 ili 1.

        for (j = 0; j < brojJedinica; ++j)
        {
            // za proveru najviseg bita znaka (2^7 = 1000 0000),
            // da bi ga ubacili na najnizi bit nakon siftovanja
            if (sifra & 0x80)
            {
                // najvisi bit je 1, ubaci na najnizi bit 1 umesto automatski stavljene 0 pri siftovanju
                sifra = (sifra << 1) | 1;
            }
            else
            {
                // najvisi bit je 0, ostavi na najnizem bitu 0 dobijenu pri siftovanju
                sifra = (sifra << 1);
            }
        }
    }

    ////////////////////////////////////////////////////////////////////
    ////////// 3. Ispis sifrovane poruke na standardni izlaz ///////////
    ////////////////////////////////////////////////////////////////////
    printf("Sifrovana poruka u hex formatu:\n");
    for (i = 0; i < n; ++i)
    {
        // %x ispisuje broj u hex obliku, 2 oznacava da se ispisuje na bar 2 decimale
        // posto je poruka[i] tipa char, nemoguce je da ima vise od 2 hex cifre
        printf("%2x ", poruka[i]);
    }

    return 0;
}

