﻿//  Napisati program na programskom jeziku C koji pronalazi i ispisuje informacije o duplim sličicama.
//  Korisnik preko standardnog ulaza unosi šifre sličica proizvoljnim redosledom (sličice su izmešane, pa
//  duplirane sličice ne moraju biti jedna za drugom). Program treba da prebroji pojavljivanja za sve
//  sličice, a zatim da izbaci iz niza sve one koje nemaju duplikate. Preostale sličice treba ispisati na
//  standardnom izlazu, u nerastućem poretku po broju pojavljivanja, tako što se za svaku sličicu u
//  posebnom redu ispiše šifra i broj pojavljivanja.Smatrati da broj različitih sličica nije veći od 1000, a
//  da je šifra ceo broj tipa int.

//  Sifra slicice je ceo broj tipa int. Ima maksimalno 1000 RAZLICITIH slicica. Treba da izbacimo one
//  koje nemaju duplikata (sve koje se ponavljaju <= 1 put), a takodje treba da sortiramo preostale
//  prema broju ponavljanja. Ovdje se koristi klasican counting sort algoritam sortiranja. Za svaki
//  element imamo po jedan brojac inicijalno postavljen na 0. Svaki put kad se naidje na isti element
//  poveca se njegov brojac za 1. Nakon toga radimo obicno sortiranje. Pri ispitivanju uslova gleda se
//  broj ponavljanja dva elementa koji se porede, a ako zakljucimo da treba da zamijene mjesta, onda
//  zamijenimo mjesta elementima. Ako smo brojeve ponavljanja cuvali u elementima, onda su i oni vec
//  zamijenjeni i to je to. Ako smo ih cuvali spolja, u nekom posebnom nizu, onda moramo i brojace za
//  ta dva elementa da zamijenimo, da bi svaki ostao uparen sa svojim elementom u nizu elemenata.
//  Ovdje koristimo drugi pristup, gdje pravimo poseban niz za brojeve ponavljanja pojedinacnih elemenata.
//  Kod counting sort algoritma vrijednost elementa je najcesce broj ulaza (indeks) u nizu brojaca, jer
//  je tako najefikasnije pristupiti brojacu (taj pristup ovdje koristimo). Zato u postavci zadatka i
//  pise da ima najvise 1000 razlicitih slicica, pa mozemo da kazemo da su im brojevi od 0 do 999 i da
//  cemo da alociramo niz od 1000 brojaca. To znaci da su validne sifre slicica od 0 do 999 (ograniceni
//  smo vrijednostima koje mogu da se unesu), ali nam nije bitno koliko elemenata se unosi (jer pamtimo
//  samo koliko puta se koji od njih 1000 unio, ne treba pojedinacno unijete elemente da pamtimo).

//  Nije nam receno koliko sifri slicica se unosi (u smislu da li se unosi odredjen broj ili dok se
//  ne unese neka specijalna vrednost), tako da mozemo da biramo da li ce korisnik da unese broj sifri,
//  pa zatim toliko sifri ili ce da unosi sifre slicica dok ne unese nevalidnu vrednost
//  (sifra < 0 || sifra >= MAX). Nacelno broj sifri nije ogranicen, nego samo broj razlicitih sifri,
//  ali cemo se drzati jednostavnog pristupa, korak po korak.

//  Za one koji zele da znaju malo vise:
//  #pragma region Ime regiona (mogu razmaci da se stave)
//  #pragma endregion
//  sluzi za definisanje regiona tako da Visual Studio dopusta da na mali minus na levoj ivici prozora
//  minimizujete blok koda koji ste okruzili sa ovim parom preprocesorskih direktiva.
//  Slobodno mozete da ugnezdite ove direktive za hijerarhijski organizovan code collapsing.
//  Ovo vazi samo za Visual Studio editor.

//  Za one koji zele da znaju malo vise:
//  _CRT_SECURE_NO_WARNINGS je preprocesorska direktiva koja govori Visual Studio C kompajleru da
//  ne upozorava korisnika da su scanf, printf i jos neke njihove varijacije nebezbedne za koriscenje.
//  Zapravo ovo je mnogo prakticnije staviti u podesavanjima projekta u preprocesorske naredbe definisane
//  u svim fajlovima. U Solution Explorer-u desni klik na zeljeni projekat, pa Properties, pa C/C++,
//  pa Preprocessor, pa u Preprocessor Definitions klik, desno ima padajuci meni, pa Edit, pa dopisete
//  u novi red _CRT_SECURE_NO_WARNINGS. Onda ne mora ni u jednom fajlu ovo rucno da se define-uje.
//  Ako koristite Visual Studio Solution/Project koje sam ja kreirao, ovaj define ispod je zasivljen
//  (kompajler ga ne vidi) jer je isljucen prethodnom linijom (ifndef). Taj ifndef ga vidi bas zato sto
//  ga vidi svaki fajl projekta nakon dodavanja u gore navedeno polje u podesavanjima.
#ifndef _CRT_SECURE_NO_WARNINGS
#define _CRT_SECURE_NO_WARNINGS
#endif

#include <stdio.h>

#define MAX 1000

int main()
{
#pragma region Varijable
    int i, j, k, n;   // indeks, indeks, indeks, broj ucitanih sifri
    int sifre[MAX] = { 0 };
    int ponavljanjaSifri[MAX] = { 0 };
    int sifra = -1; // u ovu varijablu cemo da citamo sifru sa standardnog ulaza
#pragma endregion

#pragma region Citanje i prebrojavanje cifri
    // Citanje sifri i njihovo prebrojavanje
    printf("Unesite broj sifri koje zelite da ucitate: ");
    scanf("%d", &n);
    printf("\n");

    printf("Unesite %d sifri u opsegu [0, %d]:\n", n, (MAX - 1));
    for (i = 0; i < n; ++i)
    {
        // Citaj jednu sifru
        scanf("%d", &sifra);

        if (sifra >= 0 && sifra < MAX)
        {
            // Sifra je u opsegu, dodaj je u niz, uracunaj ponavljanje
            sifre[i] = sifra;
            ++ponavljanjaSifri[sifra];
        }
        else
        {
            // Sifra nije u opsegu, ponovi citanje(vrati indeks 'i' za 1 da for petlja ponovi krug)
            --i;
        }
    }
    printf("\n");
#pragma endregion

#pragma region Izbacivanje cifri koje nemaju duplikate
    // Izbacivanje sifri koje nemaju duplikate
    // Ovo se najlakse postize sa dva indeksa, src i dst. Uvijek src indeks ide napred, a dst
    // indeks ide napred ako je u njega prepisana vrednost sa src indeksa ili ako je src == dst, pa
    // ne prepisujemo element preko njega samog (optimizacija).
    // Sa src indeksa vrednost prepisemo na dst indeks ako se vrednost na src indeksu ne izbacuje
    // i (zbog optimizacije) ako je src == dst.

    // Ovde je i <==> src, j <==> dst.
    i = j = 0;
    while (i < n)
    {
        int sifra = sifre[i];
        // Cim je neka sifra u nizu sifri, znaci da ima bar 1 ponavljanje. Ovo <= je moglo da bude ==.
        // Ubacio sam <= "iz fazona", da pokrijem sve nevalidne vrednosti.
        if (ponavljanjaSifri[sifra] <= 1)
        {
            // Izbaci sifru, samo preskoci sa src. Ili ce nekad preko dst da se prepise necim drugim
            // vrednost koja tu stoji ili dst nece ni doci dotle jer ce niz da bude kraci od dst indeksa.
            ++i;
        }
        else
        {
            // Sifra se ne izbacuje, samo treba da prepisemo sa src na dst.
            // Zbog optimizacije taj korak cemo da preskocimo ako je src == dst.
            // Ne bi trebalo nikad da bude src < dst, moze da bude src == dst do prvog izbacivanja (kad
            // src preskoci element, a dst ne) ili da bude src > dst (nakon sto se preskoci bar jedan element).
            if (i > j)
            {
                sifre[j] = sifre[i];
            }

            // Ako ne izbacujemo/preskacemo i-tu sifru, nebitno da li smo prepisivali, treba i src i dst
            // da predju na sledece pozicije, jer je i src ocitan i dst prepisan (osim mozda pri optimizaiji).
            ++i;
            ++j;
        }
    }
    // Nakon sto smo dosli do kraja niza, preskocivsi neke elemente, u novom, potencijalno skracenom, nizu
    // preostalo je onoliko elemenata dokle je stigao dst indeks. Azuriramo duzinu niza.
    n = j;
#pragma endregion

#pragma region Izbacivanje visestrukih ponavljanja cifri koje imaju duplikate
    // Ovo smemo da uradimo zato sto nema potrebe za ostatak zadatka da cuvamo originalni niz.
    // Ako bi bilo potrebno da se cuva, onda kopiramo u pomocni niz, pa iz pomocnog izbacimo
    // duplikate.
    for (i = 0; i < n; ++i)
    {
        // Ovde je opet algoritam za jednoprolazno izbacivanje svih elemenata koji ispunjavaju neki kriterijum.
        // Malopre je to bio svaki element ciji je broj ponavljanja 0, a sad je to svaki element cija je
        // vrednost jednaka sifra[i].
        // j = src, k = dst, ono sto je gore bila kombinacija (i, j) sad je (j, k), ali za svaku sifre[i] vrednost.
        j = k = i + 1;
        while (j < n)
        {
            if (sifre[i] == sifre[j])
            {
                // sifre[j] je duplikad od sifre[i], ukloni duplikat iz niza, samo ga preskoci
                ++j;
            }
            else
            {
                if (j > k)
                {
                    sifre[k] = sifre[j];
                }

                ++j;
                ++k;
            }
        }
    }
#pragma endregion

#pragma region Sortiranje slicica
    // Sortiranje slicica u nerastucem poretku (sledeca ne sme da bude veca od prethodne, moze da bude
    // manja ili jednaka).
    for (i = 0; i < n; ++i)
    {
        for (j = i + 1; j < n; ++j)
        {
            // Ako je broj ponavljanja sifre i manji od broja ponavljanja sifre j, zameni ih.
            // Nije potrebno menjati nista u ponavljanjaSifri jer se oni ne dohvataju na osnovu
            // i, j nego na osnovu elemenata niza sifre. Oni nemaju redosled kao sam niz sifri.
            int i_sifra = sifre[i];
            int j_sifra = sifre[j];

            if (ponavljanjaSifri[i_sifra] < ponavljanjaSifri[j_sifra])
            {
                int tempSifra;

                tempSifra = sifre[i];
                sifre[i] = sifre[j];
                sifre[j] = tempSifra;
            }
        }
    }
#pragma endregion

#pragma region Ispis slicica
    // Ispisi sifre slicica i broj njihovih ponavljanja
    // Sifre su sortirane nerastuce, ali ako je neka sifra unesena vise puta, ne zelimo
    // da je vise puta ispisemo.
    // Uslov za ispis je da smo prvi put naisli na neku sifru, dakle da je sifre[i] != sifre[i - 1].
    // Sad gledamo specijalne slucajeve:
    // - za kraj - nema, kad je i == n - 1, poredimo sa i - 1 == n - 2
    // - za pocetak - ima, kad je i == 0, ne mozemo da poredimo sa i - 1 == -1
    // Resenje specijalnog slucaja za pocetak je da propustimo ispis, jer sifre[0] je sifra na koju
    // smo sigurno prvi put naisli pri ispisu sifri
    printf("Slicica [sifra slicice]   x [broj ponavljanja slicice]\n\n");
    for (i = 0; i < n; ++i)
    {
        if ((i == 0) ||
            (sifre[i] != sifre[i - 1]))
        {
            printf("Slicica %5d   x %3d\n", sifre[i], ponavljanjaSifri[sifre[i]]);
        }
    }
#pragma endregion

    return 0;
}
